FROM i386/centos:7

ADD ut-server-436.tar.gz /
ADD patch451.tar.bz2 /ut-server
ADD ucc.init /ut-server/ucc.init

RUN useradd -m ut \
	&& chown ut -R /ut-server \
	&& chmod +x /ut-server/ucc /ut-server/ucc.init

EXPOSE 7777/udp
EXPOSE 5081/udp

USER ut

WORKDIR "/ut-server"

CMD ["./ucc.init", "start-fg"]
