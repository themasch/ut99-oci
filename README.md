# ut99-oci image

## how to build?

fetch required files (and hope the servers are still up):
```
$> ./fetch.sh
```

build the container:
```
$> buildah bud -t ut99 .
```


## running 

in foreground (ctrl+c to exit):
```
$> podman run --rm -it -p 7777:7777/udp ut99
```

detaching, as a service:
```
$> podman run -d -p 7777:7777/udp ut99
```

## TODO

 - config customisation (mount config file / env variables?)
 - textures need to be replaced, since the default server textures do not work with high-res client textures
 - add maps / way to add maps later (volume mount?)


## sources

 - https://wiki.unrealadmin.org/Server_Install_linux
